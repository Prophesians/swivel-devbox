#!/usr/bin/env bash

lsof -i:3000,8080 | tail +2 | awk '{ print $2 }' | xargs kill -9 $1