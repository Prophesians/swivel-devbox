#!/usr/bin/env bash

prophesiansPath="$GOPATH/src/gitlab.com/Prophesians"
swivelChromePath="$prophesiansPath/swivel-chrome"
swivelServerPath="$prophesiansPath/swivel-server"
swivelUIPath="$prophesiansPath/swivel-ui"

mkdir -p $prophesiansPath

cd $prophesiansPath

if [ ! -d "$swivelUIPath" ]; then
    git clone "git@gitlab.com:Prophesians/swivel-ui.git"
fi

cd $swivelUIPath
npm install

if [ ! -d "$swivelServerPath" ]; then
    git clone "git@gitlab.com:Prophesians/swivel-server.git"
fi

cd $swivelServerPath
glide cc;
glide install;
make build;


if [ ! -d "$swivelUIPath" ]; then
    git clone "git@gitlab.com:Prophesians/swivel-chrome.git"
fi








