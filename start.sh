#!/usr/bin/env bash

prophesiansPath="$GOPATH/src/gitlab.com/Prophesians"
swivelChromePath="$prophesiansPath/swivel-chrome"
swivelServerPath="$prophesiansPath/swivel-server"
swivelUIPath="$prophesiansPath/swivel-ui"

cd $swivelServerPath
./out/swivel-server_darwin &
serverPID=$!

cd $swivelUIPath
npm start
uiPID=$!

sleep .2
read -rsp $'Press any key to stop...\n' -n1 key
kill -9 $serverPID
kill -9 $uiPID